<?php

namespace Po4e4ka\MessageSerializer;

use http\Exception\RuntimeException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

abstract class AbstractMessageSerializer implements SerializerInterface
{
    protected bool $stamps = false;

    /**
     * Переменная, в которую записывается класс Message, который нужно сериализовать
     *
     * Пример:
     *     protected string $messageClassName = MyClassMessage::class;
     */
    protected string $messageClassName;

    /**
     * Функция для создания класса Message из данных в кафке, которые были занесены с помощью функции encodeData
     *
     * Необходимо вернуть массив аргументов класса Message, указанного в $messageClassName
     *
     * Пример:
     *     return [
     *         data['data'],
     *         data['time']
     *     ];
     *
     * !!!!! ОБЯЗАТЕЛЬНО СОБЛЮДАЙТЕ ПОРЯДОК В МАССИВЕ КАК В АРГУМЕНТАХ КОНСТРУККТОРА КЛАССА ИЗ $messageClassName
     * @param array $data
     * @return mixed
     */
    abstract protected function decodeData(array $data): array;

    /**
     * Функция кодирования класса сообщения для отправки в кафку.
     * Принимает класс сообщения.
     * Необходимо вернуть массив.
     *
     * Пример:
     *     return [
     *         'data' => $message->getData(),
     *         'time' => $message->getTime()
     *     ];
     * @param $message
     * @return array
     */
    abstract protected function encodeData($message): array;

    /**
     * Создаёт класс типа, записанного в $messageClassName с агрументами из массиа $data
     * @param $data массив агрументов
     * @return mixed
     */
    private function createMessageClass($data)
    {
        $codeString = 'return new $this->messageClassName(';
        foreach ($data as $key => $value) {
            $codeString .= '$data[\'' . $key . '\'],';
        }
        $codeString = substr($codeString, 0, -1);
        $codeString .= ');';
        return eval($codeString);;
    }

    final public function decode(array $encodedEnvelope): Envelope
    {
        $body = $encodedEnvelope['body'];
        $headers = $encodedEnvelope['headers'];
        $data = json_decode($body, true);
        try {
            $message = $this->createMessageClass($this->decodeData($data));
        } catch (\Throwable $e) {
            throw new RuntimeException('Ошибка создания класса из сообщения. Параметры: ' . json_encode($data));
        }
        if (isset($message->headers)) {
            $message->headers = $headers;
        }
        $stamps = [];
        if (isset($headers['stamps'])) {
            $stamps = unserialize($headers['stamps']);
        }
        return new Envelope($message);        
    }

    final public function encode(Envelope $envelope): array
    {
        $message = $envelope->getMessage();
        if ($message instanceof $this->messageClassName) {
            $data = $this->encodeData($message);
        } else {
            throw new \Exception('Unsupported message class');
        }
        $headers = $message->headers ?? [];
        $this->checkMessageHeaders($headers);

        if ($this->stamps) {
            $allStamps = [];
            foreach ($envelope->all() as $stamps) {
                $allStamps = array_merge($allStamps, $stamps);
            }
            $headers['stamps'] = serialize($allStamps);
        }

        return [
            'body' => json_encode($data),
            'headers' => $headers,
        ];
    }

    private function checkMessageHeaders($headers) {
        if (!is_array($headers)) {
            throw new NotEncodableValueException('$headers в классе сообщения ' . $this->messageClassName . ' должен быть массивом');
        }
    }
}

